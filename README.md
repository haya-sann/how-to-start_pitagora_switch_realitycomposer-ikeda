# How to start_PythagoraSwitch_realityComposer-ikeda

いけだじゅんじさんの「iPadでピタゴラ風からくりを作る」を受講しました。
早速例題をiPadでダウンロードして試してみました。
ところが、最初の部分で躓いてしまいました

## まず最初にサンプルファイルのダウンロード
イケダさんが用意してくれたサンプルファイルを以下のURLからダウンロードします

https://www.ikejun.jp/mosa/2205/20220528.zip




## 保存先の指定
メールやconnpass.comのお知らせで取得したURLからZIPファイルをダウンロードしたら「ダウンロード先」あるいは処理方法のオプション選択を求められる。

<img src="/images/IMG_3068.jpeg" width="400">


今回は「その他」を選び、「”ファイル”に保存」を選ぶ

<img src="/images/IMG_3069.jpeg" width="400">

保存先はiCloudの「ダウンロード」か「このiPad内」を選ぶ。

#### 私は他のプラットフォームとも共通に使いたいと思い「Dropbox」に保存したがZipファイルの解凍に失敗した。

### 素直にiCloudかiPadローカルに保存するのが良い。

<img src="/images/IMG_3070.jpeg" width="800">

## プロジェクト内で使う素材オブジェックトについて
プロジェクトファイルをインポートするだけでは中で使われているobjectの本体が読込まれていない。
単に四角い6面体が薄く表示されている。

これを実体が伴う状態にするには、objectのアイコンをタップする。すると右上にダウンロードボタンが現われるので、これをタップする。

<img src="/images/IMG_3072.jpeg" width="800">


実体のあるobjectが表示された。
<img src="/images/IMG_3073.PNG" width="800">

## Contributing
過去の動画アーカイブはこちらにあります。
https://www.youtube.com/c/ikejun

## License
このページの画像含めてご自由にお使いください
2022 Nobuo Hayashi
